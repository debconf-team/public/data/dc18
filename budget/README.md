# DebConf18 Ledger

*   The format of ledger is defined in http://ledger-cli.org/.

*   [OCF.tw ledger](https://docs.google.com/spreadsheets/d/e/2PACX-1vQgBEuYPST5zp6AYn3cHSu0nOE2vu9tM7fhvSmVNox5Xx0ve0jDn4ajA55wTzrW1JhlOCdYUqXrkbd2/pubhtml?gid=1470504038&single=true)

accounts.inc
 - List of accounts
beerbar.ledger
 - Beer accounting
budget.ledger
 - Current budget DPL approved
docs
 - Documents from goverment funds
expenses.ledger
 - Expenses
incomes.ledger
 - Incomes
journal.ledger
 - expenses + incomes
README.md
receipts
 - pictures of receipts
 - keep paper receipts for reimbursement other than SPI
 - NCHC requires quotes for video equipment rental
