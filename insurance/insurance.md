# Insurance

## Requirements

*   400 ~ 500 people
*   property / accident: 60,000 USD (1,800,000 NTD)
*   food poison

## Price

|                       | 南山                         | 富邦                         | 台產                         | 泰安                         |
|-----------------------|------------------------------|------------------------------|------------------------------|------------------------------|
|injure                 | 2,000,000 NTD (66,666 USD)   | 2,000,000 NTD (66,666 USD)   | 2,000,000 NTD (66,666 USD)   | 2,000,000 NTD (66,666 USD)   |
|accident injure        | 10,000,000 USD (333,333 USD) | 10,000,000 USD (333,333 USD) | 10,000,000 USD (333,333 USD) | 10,000,000 USD (333,333 USD) |
|accident property lost | 1,800,000 NTD (60,000 USD)   | 1,800,000 NTD (60,000 USD)   | 1,800,000 NTD (60,000 USD)   | 2,000,000 NTD (66,666 USD)   |
|maximum compensation   | 20,000,000 NTD (666,666 USD) | 20,000,000 NTD (666,666 USD) | 20,000,000 NTD (666,666 USD) | 20,000,000 NTD (666,666 USD) |
|cost                   | 14,768 NTD (492.26 USD)      | 11,041 NTD (368.03 USD)      | 11,644 NTD (388.13 USD)      | 9,402 (313.4 USD)            |

*   All insurances has the following:
    *   Food poison
    *   Additional requirement for staff member.
    *   Only include activity in NCTU, not for go out for eat and day trip.
*   泰安 has the following:
    *   Exclude terrorist.
    *   Exclude punitive damages
    *   Exclude damage caused by date related functionality.

## Day Trip

*   For day trip, we can have travel insurance. 28 NTD (1 USD) per person per day. Need to provide the following information 2 days before:
    *   Taiwanese: name, personal id, date of birth
    *   Foreigner: passport name, passport number, date of birth.
