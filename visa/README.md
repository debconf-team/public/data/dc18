# DebConf18 VISA

*   [R.O.C.(Taiwan) launches the eVisa Program on Jan. 12, 2016](https://www.taiwanembassy.org/kn_en/post/2767.html)

## Invitation Letter Process

*   Requirements:
    *   Full Name
    *   Passport No
    *   Departure Location

*   Process:
    1.  參加者發信給 debconf-team 希望取得邀請函
    2.  DebConf Visa Team 決定參加者是否可以取得邀請函
    3.  若參加者可以取得邀請函，szlin 從邀請函範本做出邀請函並簽名（PDF），轉交給 OCF 處理
    4.  OCF 用印並回傳邀請函電子檔給 szlin
    5.  szlin / DebConf Visa Team 將邀請函電子檔給參加者

*   The following link is OCF.tw document. Please help to sync the information there.
    *   <https://docs.google.com/spreadsheets/d/1H6q_jNOFmnMWkAlhrQ3PvvVFIT97WODmdNH6wStd79k/edit?usp=drive_web>

## eCode Requirement (Cannot request now, see https://lists.debian.org/debconf-discuss/2018/06/msg00001.html)

*   Nationality (3 letter country code)
*   Passport No
*   Surname
*   Given Name
*   Date of Birth (yyyy/mm/dd)
*   Date of Expiry (yyyy/mm/dd)
*   Sex (M/F/X)
